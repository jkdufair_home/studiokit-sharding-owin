﻿using Microsoft.Azure;
using Microsoft.Owin;
using StudioKit.Sharding.Extensions;
using System;

namespace StudioKit.Sharding.Owin.Extensions
{
	public static class OwinContextExtensions
	{
		private const string PersistedKey = "ShardKey";

		public static string GetShardKeyFromHost(this IOwinContext context)
		{
			var shardKey = context.Get<string>(PersistedKey);
			if (!string.IsNullOrWhiteSpace(shardKey))
				return shardKey;

			var uri = context.Request.Uri;
			if (uri.HostNameType != UriHostNameType.Dns)
				return null;

			// the default value is expected to throw a ShardManagementException when used as a ShardKey
			shardKey = ShardDomainConstants.InvalidShardKey;

			var host = uri.DnsSafeHost;
			if (host == ShardDomainConstants.Localhost)
			{
				var localhostShardKey = CloudConfigurationManager.GetSetting(ShardAppSetting.LocalhostShardKey, false);
				if (!string.IsNullOrWhiteSpace(localhostShardKey))
					return localhostShardKey;

				// localhost defaults to Purdue
				shardKey = ShardDomainConstants.PurdueShardKey;
			}
			else
			{
				// Shibboleth callback => use query param
				if (context.Request.Query.Get("shardKey") != null &&
					(uri.AbsoluteUri.EndsWith("/api/account/shibboleth") ||
					uri.AbsoluteUri.EndsWith("/api/account/externalLoginCallback")))
					return context.Request.Query.Get("shardKey");

				// Azure URLs => treat as Purdue domain
				if (host.Contains(ShardDomainConstants.AzureCloudServiceHostname) || host.Contains(ShardDomainConstants.AzureWebAppHostname))
					return ShardDomainConstants.PurdueShardKey;

				// Root Domains
				var hostParts = host.Split('.');
				if (ShardDomainConfiguration.Instance.RootDomainParts.Contains(hostParts[0]))
					return ShardDomainConstants.RootShardKey;

				// Use subdomain
				var subdomainShardKey = host
					.Replace($".{ShardDomainConfiguration.Instance.Hostname}", "")
					.FromSubdomainToShardKey();
				if (!string.IsNullOrWhiteSpace(subdomainShardKey))
					shardKey = subdomainShardKey;
			}

			context.Set(PersistedKey, shardKey);

			return shardKey;
		}

		public static string GetWebUrlFromRequest(this IOwinContext context)
		{
			var uri = context.Request.Uri;
			if (uri.HostNameType != UriHostNameType.Dns)
				return null;

			var host = uri.DnsSafeHost;
			if (host == ShardDomainConstants.Localhost)
				return "http://localhost:3000";

			if (host.Contains(ShardDomainConstants.AzureCloudServiceHostname) ||
				host.Contains(ShardDomainConstants.AzureWebAppHostname) ||
				!ShardDomainConfiguration.Instance.Hostname.Contains("api."))
				return $"{uri.Scheme}://{host}";

			var shardKey = context.GetShardKeyFromHost();
			return ShardDomainConfiguration.Instance.BaseUrlWithShardKey(shardKey).Replace("api.", "");
		}
	}
}